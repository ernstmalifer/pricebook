'use strict';

//Products service used to communicate Products REST endpoints
// angular.module('products').factory('Products', ['$resource',
// 	function($resource) {
// 		return $resource('products/:productId', { productId: '@_id'
// 		}, {
// 			update: {
// 				method: 'PUT'
// 			}
// 		});
// 	}
// ]);

// 'use strict';

//Categories service used to communicate Categories REST endpoints
angular.module('products').factory('Categories', ['$resource',
  function($resource) {
    return $resource('categories/:categoryId', { categoryId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);