'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = 'pricebook';
	var applicationModuleVendorDependencies = ['ngResource', 'ngCookies',  'ngAnimate',  'ngTouch',  'ngSanitize',  'ui.router', 'ui.bootstrap', 'ui.utils', 'lr.upload', 'rzModule', 'ngCkeditor'];

	// Add a new vertical module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();
'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.hashPrefix('!');
	}
]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

  // Fixing google bug with redirect
  if (window.location.href[window.location.href.length - 1] === '#' &&
      // for just the error url (origin + /#)
      (window.location.href.length - window.location.origin.length) === 2) {
      window.location.href = window.location.origin + '/#!';
  }

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
'use strict';

// Use application configuration module to register a new module
ApplicationConfiguration.registerModule('admin');

'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('categories');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('logs');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('products');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users');
'use strict';

// Configuring the Articles module
angular.module('admin').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Admin', 'admin', 'dropdown', '/admin', null, ['admin']);
		Menus.addSubMenuItem('topbar', 'admin', 'Users', 'admin/users/list');
	}
]);

'use strict';

//Setting up route
angular.module('admin').config(['$stateProvider',
	function($stateProvider) {
		// Admins state routing
		$stateProvider.
		state('admin-users', {
			url: '/admin/users',
			templateUrl: 'modules/admin/views/users/main-users.client.view.html',
			controller: 'AdminUsersController'
		}).
		state('admin-users.list', {
			url: '/list',
			templateUrl: 'modules/admin/views/users/list-users.client.view.html',
			controller: 'AdminUsersListController'
		}).
		state('admin-users.edit', {
			url: '/edit',
			templateUrl: 'modules/admin/views/users/edit-user.client.view.html',
			controller: 'AdminUsersEditController'
		}).
		state('admin-users.view', {
			url: '/view',
			templateUrl: 'modules/admin/views/users/view-user.client.view.html',
			controller: 'AdminUsersViewController'
		});
	}
]);

'use strict';

// Admins controller
angular.module('admin')
	.controller('AdminUsersListController', ['$scope', '$filter', '$state', 'Admin',
		function($scope, $filter, $state, Admin) {
			// Find a list of Users
			$scope.find = function() {
				Admin.query({type: 'users'}, function (data) {
					$scope.users = data;
					$scope.buildPager();
				});

			};

			$scope.find();

			$scope.buildPager = function () {
				$scope.pagedItems = [];
				$scope.itemsPerPage = 2;
				$scope.currentPage = 1;
				$scope.figureOutItemsToDisplay();
			};

			$scope.figureOutItemsToDisplay = function () {
				$scope.filteredItems = $filter('filter')($scope.users, { $: $scope.search});
				$scope.filterLength = $scope.filteredItems.length;
				var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
				var end = begin + $scope.itemsPerPage;
				$scope.pagedItems = $scope.filteredItems.slice(begin, end);
			};

			$scope.pageChanged = function() {
				$scope.figureOutItemsToDisplay();
			};

		}
	])
	.controller('AdminUsersViewController', ['$scope', '$state', 'Admin',
		function($scope, $state, Admin) {
			if (!$scope.user) {
				$state.go('admin-users.list');
			}

			// Remove existing Admin
			$scope.remove = function(admin) {
				if (confirm('Are you sure you want to Delete?')) {
					Admin.delete({type: 'users', id: $scope.user._id}, $scope.user, function (data) {
						$scope.users.splice($scope.users.indexOf($scope.user), 1);
						$scope.user = {};
						$state.go('admin-users.list');
					}, function(errorResponse) {
						$scope.error = errorResponse.data.message;
					});
				}
			};
		}
	])
	.controller('AdminUsersEditController', ['$scope', '$state', 'Admin',
		function($scope, $state, Admin) {

		//Redirect if user is not set.
		if (!$scope.user) {
			$state.go('admin-users.list');
		}

		// Update existing Admin
		$scope.update = function() {

			Admin.update({type: 'users', id: $scope.user._id}, $scope.user, function (data) {
				$state.go('admin-users.view');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

		};

		$scope.toggleRole = function (role) {
			if ($scope.user.roles.indexOf(role) === -1) {
				$scope.user.roles.push(role);
			}
			else {
				$scope.user.roles.splice($scope.user.roles.indexOf(role), 1);
			}
		};

	}])
	.controller('AdminUsersController', ['$scope', '$state', '$stateParams', '$location', 'Authentication', 'Admin',
	function($scope, $state, $stateParams, $location, Authentication, Admin) {
		//Redirect if not sub route
		if ($state.current.name === 'admin-users') {
			$state.go('admin-users.list');
		}

		//Redirect if not Admin
		if(!Authentication.isAdmin()) {
			$state.go('home');
		}

		$scope.users = [];
		$scope.user = undefined;
		$scope.authentication = Authentication;


		$scope.setUser = function (user) {
			$scope.user = user;
		};

		// Find existing Admin
		$scope.findOne = function() {
			$scope.user = Admin.get({
				type: 'users',
				id: $stateParams.userId
			});
		};
	}
]);

'use strict';

//Admins service used to communicate Admins REST endpoints
angular.module('admin').factory('Admin', ['$resource',
	function($resource) {
		return $resource('admin/:type/:id', { type: '@type', id: '@id'}, {
			update: {
				method: 'PUT'
			},
			get: {
				method: 'GET'
			},
			delete: {
				method: 'DELETE'
			}
		});
	}
]);

'use strict';

// Configuring the Articles module
angular.module('categories').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Categories', 'categories', 'dropdown', '/categories(/create)?');
		Menus.addSubMenuItem('topbar', 'categories', 'List Categories', 'categories');
		Menus.addSubMenuItem('topbar', 'categories', 'New Category', 'categories/create');
	}
]);
'use strict';

//Setting up route
angular.module('categories').config(['$stateProvider',
	function($stateProvider) {
		// Categories state routing
		$stateProvider.
		state('listCategories', {
			url: '/categories',
			templateUrl: 'modules/categories/views/list-categories.client.view.html'
		}).
		state('createCategory', {
			url: '/categories/create',
			templateUrl: 'modules/categories/views/create-category.client.view.html'
		}).
		state('viewCategory', {
			url: '/categories/:categoryId',
			templateUrl: 'modules/categories/views/view-category.client.view.html'
		}).
		state('editCategory', {
			url: '/categories/:categoryId/edit',
			templateUrl: 'modules/categories/views/edit-category.client.view.html'
		});
	}
]);
'use strict';

// Categories controller
angular.module('categories').controller('CategoriesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Categories',
	function($scope, $stateParams, $location, Authentication, Categories) {
		$scope.authentication = Authentication;

		$scope.fields = [];

		//Redirect if not Registered
		if(!Authentication.user) {
			$state.go('home');
		}

		// Create new Category
		$scope.create = function() {
			// Create new Category object
			var category = new Categories (_.extend({
				name: this.name,
				permalink: this.permalink
			}, {fields: $scope.fields}));

			// Redirect after save
			category.$save(function(response) {
				$location.path('categories/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Add new Field
		$scope.addField = function(type) {
			switch(type){
				case 'text':
					$scope.fields.push({name: '', uri: '', type: 'text'});
					break;
				case 'textarea':
					$scope.fields.push({name: '', uri: '', type: 'textarea'});
					break;
				case 'number':
					$scope.fields.push({name: '', uri: '', type: 'number'});
					break;
				case 'selection':
					$scope.fields.push({name: '', uri: '', type: 'selection', selectiontype: 'single', selections: []});
					break;
				case 'image':
					$scope.fields.push({name: '', uri: '', type: 'image'});
					break;
				case 'derived':
					$scope.fields.push({name: '', uri: '', type: 'derived'});
					break;
			}
		};

		$scope.removeField = function(index){
			$scope.fields.splice(index, 1);
		};

		$scope.addSelection = function(index, value, label ){
			$scope.fields[index].toAddValue = '';
			$scope.fields[index].toAddLabel = '';
			$scope.fields[index].selections.push({value: value, label: label});
		};

		// Remove existing Category
		$scope.remove = function(category) {
			if ( category ) { 
				category.$remove();

				for (var i in $scope.categories) {
					if ($scope.categories [i] === category) {
						$scope.categories.splice(i, 1);
					}
				}
			} else {
				$scope.category.$remove(function() {
					$location.path('categories');
				});
			}
		};

		// Update existing Category
		$scope.update = function() {
			// Clean fields
			$scope.category = _.omit($scope.category,'fields');

			// var category = _.extend($scope.category, {fields: $scope.fields});
			var category = $scope.category;
			category.fields = $scope.fields;

			category.$update(function() {
				$location.path('categories/' + category._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Categories
		$scope.find = function() {
			$scope.categories = Categories.query();
		};

		// Find existing Category
		$scope.findOne = function() {
			$scope.category = Categories.get({ 
				categoryId: $stateParams.categoryId
			});

			$scope.category.$promise.then(function (result) {
			    $scope.fields = $scope.category.fields;
			});

		};

		$scope.updateURI = function(from, index){
			var re = /[^a-z0-9]+/gi;
	    var re2 = /^-*|-*$/g;
	    from = from.replace(re, '-').toLowerCase();
			eval('$scope.fields'+ '[' + index + '].uri = \'' + from + '\'');
		};

		$scope.updatePermalink = function(str, isNew) {
				if(str !== undefined){
			    var re = /[^a-z0-9]+/gi; // global and case insensitive matching of non-char/non-numeric
			    var re2 = /^-*|-*$/g;     // get rid of any leading/trailing dashes
			    str = str.replace(re, '-');  // perform the 1st regexp
			    // return str.replace(re2, '').toLowerCase(); // ..aaand the second + return lowercased result
			    if(isNew){
			    	$scope.permalink = '';
			    	$scope.permalink = str.replace(re2, '').toLowerCase(); // ..aaand the second + return lowercased result
			    } else {
			    	$scope.category.permalink = str.replace(re2, '').toLowerCase(); // ..aaand the second + return lowercased result
			    }
				} else {
					$scope.permalink = '';
				}
		};

		$scope.updateDerived = function(category){
			
		};
		
	}
]);
'use strict';

//Categories service used to communicate Categories REST endpoints
angular.module('categories').factory('Categories', ['$resource',
	function($resource) {
		return $resource('categories/:categoryId', { categoryId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		});
	}
]);
'use strict';

angular.module('core').controller('HeaderController', ['$scope', 'Authentication', 'Menus',
	function($scope, Authentication, Menus) {
		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.menu = Menus.getMenu('topbar');

		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});
	}
]);
'use strict';


angular.module('core').controller('HomeController', ['$scope', 'Authentication',
	function($scope, Authentication) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
	}
]);
'use strict';

//Menu service used for managing  menus
angular.module('core').service('Menus', [

	function() {
		// Define a set of default roles
		this.defaultRoles = ['*'];

		// Define the menus object
		this.menus = {};

		// A private function for rendering decision 
		var shouldRender = function(user) {
			if (user) {
				if (!!~this.roles.indexOf('*')) {
					return true;
				} else {
					for (var userRoleIndex in user.roles) {
						for (var roleIndex in this.roles) {
							if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
								return true;
							}
						}
					}
				}
			} else {
				return this.isPublic;
			}

			return false;
		};

		// Validate menu existance
		this.validateMenuExistance = function(menuId) {
			if (menuId && menuId.length) {
				if (this.menus[menuId]) {
					return true;
				} else {
					throw new Error('Menu does not exists');
				}
			} else {
				throw new Error('MenuId was not provided');
			}

			return false;
		};

		// Get the menu object by menu id
		this.getMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			return this.menus[menuId];
		};

		// Add new menu object by menu id
		this.addMenu = function(menuId, isPublic, roles) {
			// Create the new menu
			this.menus[menuId] = {
				isPublic: isPublic || false,
				roles: roles || this.defaultRoles,
				items: [],
				shouldRender: shouldRender
			};

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			delete this.menus[menuId];
		};

		// Add menu item object
		this.addMenuItem = function(menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Push new menu item
			this.menus[menuId].items.push({
				title: menuItemTitle,
				link: menuItemURL,
				menuItemType: menuItemType || 'item',
				menuItemClass: menuItemType,
				uiRoute: menuItemUIRoute || ('/' + menuItemURL),
				isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].isPublic : isPublic),
				roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].roles : roles),
				position: position || 0,
				items: [],
				shouldRender: shouldRender
			});

			// Return the menu object
			return this.menus[menuId];
		};

		// Add submenu item object
		this.addSubMenuItem = function(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
					// Push new submenu item
					this.menus[menuId].items[itemIndex].items.push({
						title: menuItemTitle,
						link: menuItemURL,
						uiRoute: menuItemUIRoute || ('/' + menuItemURL),
						isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].items[itemIndex].isPublic : isPublic),
						roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].items[itemIndex].roles : roles),
						position: position || 0,
						shouldRender: shouldRender
					});
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenuItem = function(menuId, menuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
					this.menus[menuId].items.splice(itemIndex, 1);
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeSubMenuItem = function(menuId, submenuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
					if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
						this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
					}
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		//Adding the topbar menu
		this.addMenu('topbar');
	}
]);
'use strict';

// Configuring the Articles module
angular.module('logs').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Logs', 'logs', 'dropdown', '/logs(/create)?');
		Menus.addSubMenuItem('topbar', 'logs', 'List Logs', 'logs');
		Menus.addSubMenuItem('topbar', 'logs', 'New Log', 'logs/create');
	}
]);
'use strict';

//Setting up route
angular.module('logs').config(['$stateProvider',
	function($stateProvider) {
		// Logs state routing
		$stateProvider.
		state('listLogs', {
			url: '/logs',
			templateUrl: 'modules/logs/views/list-logs.client.view.html'
		}).
		state('createLog', {
			url: '/logs/create',
			templateUrl: 'modules/logs/views/create-log.client.view.html'
		}).
		state('viewLog', {
			url: '/logs/:logId',
			templateUrl: 'modules/logs/views/view-log.client.view.html'
		}).
		state('editLog', {
			url: '/logs/:logId/edit',
			templateUrl: 'modules/logs/views/edit-log.client.view.html'
		});
	}
]);
'use strict';

// Logs controller
angular.module('logs').controller('LogsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Logs',
	function($scope, $stateParams, $location, Authentication, Logs) {
		$scope.authentication = Authentication;

		//Redirect if not Registered
		if(!Authentication.user) {
			$state.go('home');
		}

		// Create new Log
		$scope.create = function() {
			// Create new Log object
			var log = new Logs ({
				name: this.name
			});

			// Redirect after save
			log.$save(function(response) {
				$location.path('logs/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Log
		$scope.remove = function(log) {
			if ( log ) { 
				log.$remove();

				for (var i in $scope.logs) {
					if ($scope.logs [i] === log) {
						$scope.logs.splice(i, 1);
					}
				}
			} else {
				$scope.log.$remove(function() {
					$location.path('logs');
				});
			}
		};

		// Update existing Log
		$scope.update = function() {
			var log = $scope.log;

			log.$update(function() {
				$location.path('logs/' + log._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Logs
		$scope.find = function() {
			$scope.logs = Logs.query();
		};

		// Find existing Log
		$scope.findOne = function() {
			$scope.log = Logs.get({ 
				logId: $stateParams.logId
			});
		};
	}
]);
'use strict';

//Logs service used to communicate Logs REST endpoints
angular.module('logs').factory('Logs', ['$resource',
	function($resource) {
		return $resource('logs/:logId', { logId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Configuring the Articles module
angular.module('products').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Products', 'products', 'dropdown', '/products(/create)?');
		Menus.addSubMenuItem('topbar', 'products', 'List Products', 'products');
		Menus.addSubMenuItem('topbar', 'products', 'New Product', 'products/create');
	}
]);
'use strict';

//Setting up route
angular.module('products').config(['$stateProvider',
	function($stateProvider) {
		// Products state routing
		$stateProvider.
		state('listProducts', {
			url: '/products',
			templateUrl: 'modules/products/views/list-products.client.view.html'
		}).
		state('createProduct', {
			url: '/products/create',
			templateUrl: 'modules/products/views/create-product.client.view.html'
		}).
		state('viewProduct', {
			url: '/products/:productId',
			templateUrl: 'modules/products/views/view-product.client.view.html'
		}).
		state('viewProductCategory', {
			url: '/products/category/:categoryId',
			templateUrl: 'modules/products/views/list-products.client.view.html'
		}).
		state('editProduct', {
			url: '/products/:productId/edit',
			templateUrl: 'modules/products/views/edit-product.client.view.html'
		});
	}
]);
'use strict';

// Products controller
angular.module('products').controller('ProductsController', ['$scope', '$stateParams', '$state', '$timeout', '$sce', '$location', 'Authentication', 'Products', 'Categories', 'Logs',
	function($scope, $stateParams, $state, $timeout, $sce, $location, Authentication, Products, Categories, Logs) {
		$scope.authentication = Authentication;

		//Redirect if not Registered
		if(!Authentication.user) {
			$state.go('home');
		}

		// Create new Product
		$scope.create = function() {
			// Create new Product object
			var product = new Products ({
				name: this.name,
				category: this.category,
				status: this.status,
				image: this.image,
				price: this.price,
				sellprice: this.sellprice,
				fields: []
			});

			_.each($scope.fields, function(field){
				var fieldObject = {};
				fieldObject[field.uri] = field.value;
				product.fields.push(fieldObject);
				// _.extend(product, fieldObject);
			});

			product.$save(function(response) {
				// $location.path('products/' + response._id);
				$location.path('products/category/' + product.category);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Product
		$scope.remove = function(product) {
			if ( product ) { 
				product.$remove();

				for (var i in $scope.products) {
					if ($scope.products [i] === product) {
						$scope.products.splice(i, 1);
					}
				}
			} else {
				$scope.product.$remove(function() {
					$location.path('products');
				});
			}
		};

		// Update existing Product
		$scope.update = function() {
			var product = $scope.product;

			product.name = product.name;
			product.status = product.status;
			product.category = product.category;
			product.image = product.image;
			product.price = product.price;
			product.sellprice = product.sellprice;
			product.fields = [];

			_.each($scope.fields, function(field){
				var fieldObject = {};
				fieldObject[field.uri] = field.value;
				product.fields.push(fieldObject);
				// _.extend(product, fieldObject);
			});

			// console.log(product);

			product.$update(function() {
				// $location.path('products/' + product._id);
				$location.path('products/category/' + product.category);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Products
		$scope.find = function() {
			$scope.products = Products.query();
		};

		// Find a list of Categories
		$scope.findCategories = function() {
			$scope.categories = Categories.query();

			$scope.categories.$promise.then(function(resp){
				$scope.searchCategorySelected = {};
				$scope.searchCategorySelected.name = $scope.categories[0].permalink;

				if($stateParams.categoryId){
					var categoryId = _.find($scope.categories, function(category){return category.permalink == $stateParams.categoryId});
					$scope.searchCategorySelected.name = categoryId.permalink;
					$scope.searchCategorySelectedChanged();
				} else {
					$scope.searchCategorySelectedChanged();
				}

			});
		};

		// Find existing Product
		$scope.findOne = function() {
			$scope.product = Products.get({ 
				productId: $stateParams.productId
			});
			$scope.product.$promise.then(function(resp){
				$scope.category = $scope.product.category;
				$scope.updateCategory();

				_.each($scope.product.fields, function(field, index){

					var key = _.keys(field)[0];
					var value =_.values(field)[0];
					
					// Fuse to fields
					$scope.categories.$promise.then(function(resp){
						$scope.fields = _.each($scope.fields, function(field){ if(field.uri === key){ field.value = value; } });
					});
				});
			});


			$scope.logs = Logs.query({productId: $stateParams.productId});
		};

		$scope.updateCategory = function(){
			$scope.categories.$promise.then(function(resp){
				var categorySelected = _.find($scope.categories, function(category){return category.permalink === $scope.category;});
				$scope.fields = categorySelected.fields;
			});
		};

		$scope.derive = function(deriveFunction, index){
			var val = deriveFunction.substring(deriveFunction.lastIndexOf('<%= ')+4,deriveFunction.lastIndexOf(' %>'));

			var deriveFunctionReplaced = deriveFunction.replace(val,'val');

			var value = 0;
			// If 0 == price
			if(val === '0'){
				value = $scope.product.price;
			} else {
				value = $scope.fields[val-1].value;
			}

			if(!isNaN(value) && value.length !== 0){
				var compiled = _.template(deriveFunctionReplaced);
				$scope.fields[index].value = eval(compiled({val: value})).toFixed(2);
				return eval(compiled({val: value})).toFixed(2);
			} else {
				$scope.fields[index].value = '0.00';
				return '0.00';
			}
		};

		$scope.updateSellPrice = function(){
			$scope.sellprice = ($scope.price / (1 - .25)).toFixed(2);
		};

		$scope.updateSellPriceU = function(){
			$scope.product.sellprice = ($scope.product.price / (1 - .25)).toFixed(2);
		};

		$scope.onSuccess = function(resp, update){
			if(update){
				$scope.product.image = resp.data.filename;
				$scope.productimageupdate = true;
			} else {
				$scope.image = resp.data.filename;
			}
		};

		$scope.searchInit = function(){
			$scope.allFields = [];
			$scope.allUniqueFields = [];
			$scope.allDuplicateFields = [];

			

			$scope.products.$promise.then(function(resp){
				$scope.categories.$promise.then(function(resp){
					// For each category, get all fields and fuse
					_.each($scope.categories, function(category){
						_.each(category.fields, function(field){
							$scope.allFields.push(field.uri);
						});
					});

					// console.log($scope.allFields);
					// Remove duplicates
					$scope.allUniqueFields = _.uniq($scope.allFields);
					// console.log($scope.allUniqueFields);

					// Get duplicates
					var count = _.countBy($scope.allFields, _.identity);
					_.chain(count).keys().each(function(countitemkey){ if(count[countitemkey] > 1){ $scope.allDuplicateFields.push(countitemkey); }});
					// console.log($scope.allDuplicateFields);
					$scope.updateCriteria();

				});
			});
		};

		$scope.$on("slideEnded", function() {
		  $scope.updateCriteria();
		  $scope.$apply()
		});

		$scope.sprice = { min: 0, max: 200000};
		$scope.ssellprice = { min: 0, max: 200000};

		$scope.checkAll = function(fieldSelections){
			_.each(fieldSelections, function(fieldSelection){
				fieldSelection.selected = true;
			});
			$scope.updateCriteria();
		};

		$scope.unCheckAll = function(fieldSelections){
			_.each(fieldSelections, function(fieldSelection){
				fieldSelection.selected = false;
			});
			$scope.updateCriteria();
		};

		$scope.selectDeselectAllProducts = function(){
			_.each($scope.products, function(product){
				product.selected = $scope.productsSelectedAll;
			});
		};

		$scope.updateCriteria = function(){

			$scope.searchCriteria = { fields: [] };

			var selectedCategory = _.find($scope.categories, function(category){
				return category.permalink === $scope.searchCategorySelected.name;
			});

			_.each(selectedCategory.fields, function(field){
				if(field.searchable){
					switch(field.type){
						case 'text':
							$scope.searchCriteria.fields.push({name: field.uri, type: field.type, text: field.value})
							break;
						case 'number':
							$scope.searchCriteria.fields.push({name: field.uri, type: field.type, minval: parseInt(field.minval), maxval: parseInt(field.maxval)})
							break;
						case 'derived':
							$scope.searchCriteria.fields.push({name: field.uri, type: field.type, minval: parseInt(field.minval), maxval: parseInt(field.maxval)})
							break;
						case 'textarea':
							$scope.searchCriteria.fields.push({name: field.uri, type: field.type, text: field.value})
							break;
						case 'selection':
							var selections = [];
							_.each(field.selections, function(selection){
								if(selection.selected){ selections.push(selection); }
							})
							$scope.searchCriteria.fields.push({name: field.uri, type: field.type, selections: selections})
							break;
					}
				}
			});

			// console.log(selectedCategory, $scope.searchCriteria)

			// console.log($scope.searchCriteria)
			// $scope.searchCriteria = searchCriteria;
		};

		$scope.categoryFilter = function(product){
			if($scope.searchCategorySelected){
				return product.category === $scope.searchCategorySelected.name;
			} else {
				return false;
			}
		};

		$scope.sortBy = function(predicate, index){
      $scope.products.sort(function(x,y){

      	// Sort by fields
      	if(index !== undefined) {
      		var result = compareValues(x.fields[index][predicate], y.fields[index][predicate]);
	      	return result === 0
	      		? compareValues(y.fields[index][predicate], x.fields[index][predicate])
	      		: result;
      	} else {
      		var result = compareValues(x[predicate], y[predicate]);
	      	return result === 0
	      		? compareValues(y[predicate], x[predicate])
	      		: result;
      	}
      	

      })

      // console.log($scope.products)
    }

    function compareValues(x, y){
    	return (x > y)
    		? 1
    		: (x < y ? -1 : 0);
    };

		$scope.criteriaFilter = function(product){

			var flag = true;

			if(product === undefined){ return false; }

			if(!isNaN($scope.sprice.minval) && !isNaN($scope.sprice.maxval)){
				if(product.price < $scope.sprice.minval){
					flag = false;
				}
				if(product.price > $scope.sprice.maxval){
					flag = false;
				}
			}

			if(!isNaN($scope.ssellprice.minval) && !isNaN($scope.ssellprice.maxval)){
				if(product.sellprice < $scope.ssellprice.minval){
					flag = false;
				}
				if(product.sellprice > $scope.ssellprice.maxval){
					flag = false;
				}
			}


			_.each($scope.searchCriteria.fields, function(field){

				var uriToCompare = field.name;
				var uriToCompareType = field.type;


				var productFieldToCompare = _.find(product.fields, function(productfield){
					return uriToCompare === _.keys(productfield)[0];
				});

				// console.log(field)

				switch(uriToCompareType) {
					case 'image':
						break;
					case 'text':
						break;
					case 'description':
						break;
					case 'selection':
					// console.log(field.selections, productFieldToCompare)
					// console.log(_.contains(_.pluck(field.selections, 'value'), productFieldToCompare[uriToCompare]))
						if(!_.isEmpty(field.selections)){
							if(!_.contains(_.pluck(field.selections, 'value'), productFieldToCompare[uriToCompare])){
								// console.log(_.pluck(field.selections, 'value'), productFieldToCompare[uriToCompare]);
								flag = false;
							}
						} else {
							flag = false;
						}
						break;
					case 'number':
					// console.log(isNaN(field.minval))
						if(!isNaN(field.minval) && !isNaN(field.maxval)){
							if(productFieldToCompare[uriToCompare] < field.minval){
								// console.log('2');
								flag = false;
							}
							if(productFieldToCompare[uriToCompare] > field.maxval){
								// console.log('3');
								flag = false;
							}
						}
						break;
					case 'derived':
						if(!isNaN(field.minval) && !isNaN(field.maxval)){
							if(productFieldToCompare !== undefined){
								if(productFieldToCompare[uriToCompare] < field.minval){
									// console.log('4');
									flag = false;
								}
								if(productFieldToCompare[uriToCompare] > field.maxval){
									// console.log('5');
									flag = false;
								}
							} else {
								flag = true;
							}
							
						}
						break;
					default:
				}

			})

			// console.log(flag);
			return flag;
		}

		$scope.searchCategorySelectedChanged = function(){
			$scope.categories.$promise.then(function(resp){
				$scope.columns = [];
				var category = _.find($scope.categories, function(category){ return category.permalink === $scope.searchCategorySelected.name; })
				_.chain(category.fields).pluckMany('name','uri').each(function(field){
					var selected = true;
					if(field.uri === 'specifications' || field.uri === 'notes' || field.uri === 'warranty-support' || field.uri === 'description') {
						selected = false;
					}
					$scope.columns.push({name: field.name, uri: field.uri, selected: selected});
				})
			})
		};

		$scope.setProductRowClass = function(status){
			switch(status){
				case 'updated':
					return 'danger';
				case 'for approval':
					return 'warning';
				case 'approved':
					return '';
			}
		};

		$scope.setProductRowTag = function(status){
			switch(status){
				case 'updated':
					return 'danger';
				case 'for approval':
					return 'warning';
				case 'approved':
					return 'success';
			}
		};

		$scope.isColumnShown = function(key){
			var column = _.find($scope.columns, function(column){ return _.keys(key)[0] === column.uri;})
			if(column !== undefined){
				return column.selected;
			} else {
				return false;
			}
		};

		$scope.getColumnValue = function(product, fielduri){
			// console.log(product.fields, fielduri)
			var fieldObject = _.find(product.fields, function(field){
				return _.keys(field)[0] === fielduri
			});
			return _.values(fieldObject)[0];
		}

		$scope.formatField = function(field){

			var key = _.keys(field)[0];

			var selectedCategory = _.find($scope.categories, function(category){
				return category.permalink === $scope.searchCategorySelected.name;
			});

			var selectedField = _.find(selectedCategory.fields, function(field){
				return field.uri == key;
			})

			if(field){
				switch(selectedField.type) {
					case 'description':
						return '<small>' + field[key] + '</small>';
					case 'number':
						var returnNumber = '';
						if(selectedField.prefix){ returnNumber += selectedField.prefix; }
						returnNumber += field[key];
						if(selectedField.postfix){ returnNumber += selectedField.postfix; }
						
						return returnNumber;
					case 'derived':
						var returnNumber = '';
						if(selectedField.prefix){ returnNumber += selectedField.prefix; }
						returnNumber += field[key];
						if(selectedField.postfix){ returnNumber += selectedField.postfix; }
						
						return returnNumber;

					default:
						return field[key];
				}
			}
		};

		$scope.updatedFormat = function(date){
			return moment(date).fromNow();
		}

		$scope.formatLog = function(updates){
			var string = '';
			_.each(updates, function(update, index){
				string += update.key;
				if(index == updates.length - 1){
					string += ''
				} else {
					string += ', '
				}
			});

			return string;
		}

		$scope.go = function ( path ) {
		  $location.path( path );
		};

	}
		
]);

String.prototype.capitalize = function() {
    return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};

// Pluck extend
_.mixin({ 
    pluckMany: function() {
        var args = _.rest(arguments, 1);
        return _.map(arguments[0], function(item) {
            var obj = {};
            _.each(args, function(arg) {
                obj[arg] = item[arg]; 
            });
            return obj;
        });
    }
})
'use strict';

//Products service used to communicate Products REST endpoints
// angular.module('products').factory('Products', ['$resource',
// 	function($resource) {
// 		return $resource('products/:productId', { productId: '@_id'
// 		}, {
// 			update: {
// 				method: 'PUT'
// 			}
// 		});
// 	}
// ]);

// 'use strict';

//Categories service used to communicate Categories REST endpoints
angular.module('products').factory('Categories', ['$resource',
  function($resource) {
    return $resource('categories/:categoryId', { categoryId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
'use strict';

//Products service used to communicate Products REST endpoints
// angular.module('products').factory('Products', ['$resource',
// 	function($resource) {
// 		return $resource('products/:productId', { productId: '@_id'
// 		}, {
// 			update: {
// 				method: 'PUT'
// 			}
// 		});
// 	}
// ]);

// 'use strict';

//Logs service used to communicate Logs REST endpoints
angular.module('products').factory('Logs', ['$resource',
  function($resource) {
    return $resource('logs/:logId', { logId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
'use strict';

//Products service used to communicate Products REST endpoints
angular.module('products').factory('Products', ['$resource',
	function($resource) {
		return $resource('products/:productId', { productId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Config HTTP Error Handling
angular.module('users').config(['$httpProvider',
	function($httpProvider) {
		// Set the httpProvider "not authorized" interceptor
		$httpProvider.interceptors.push(['$q', '$location', 'Authentication',
			function($q, $location, Authentication) {
				return {
					responseError: function(rejection) {
						switch (rejection.status) {
							case 401:
								// Deauthenticate the global user
								Authentication.user = null;

								// Redirect to signin page
								$location.path('signin');
								break;
							case 403:
								// Add unauthorized behaviour 
								break;
						}

						return $q.reject(rejection);
					}
				};
			}
		]);
	}
]);
'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('profile', {
			url: '/settings/profile',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('password', {
			url: '/settings/password',
			templateUrl: 'modules/users/views/settings/change-password.client.view.html'
		}).
		state('accounts', {
			url: '/settings/accounts',
			templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
		}).
		state('signup', {
			url: '/signup',
			templateUrl: 'modules/users/views/authentication/signup.client.view.html'
		}).
		state('signin', {
			url: '/signin',
			templateUrl: 'modules/users/views/authentication/signin.client.view.html'
		}).
		state('forgot', {
			url: '/password/forgot',
			templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
		}).
		state('reset-invalid', {
			url: '/password/reset/invalid',
			templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
		}).
		state('reset-success', {
			url: '/password/reset/success',
			templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
		}).
		state('reset', {
			url: '/password/reset/:token',
			templateUrl: 'modules/users/views/password/reset-password.client.view.html'
		});
	}
]);
'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication',
	function($scope, $http, $location, Authentication) {
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		$scope.signup = function() {
			$http.post('/auth/signup', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		$scope.signin = function() {
			$http.post('/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);
'use strict';

angular.module('users').controller('PasswordController', ['$scope', '$stateParams', '$http', '$location', 'Authentication',
	function($scope, $stateParams, $http, $location, Authentication) {
		$scope.authentication = Authentication;

		//If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		// Submit forgotten password account id
		$scope.askForPasswordReset = function() {
			$scope.success = $scope.error = null;

			$http.post('/auth/forgot', $scope.credentials).success(function(response) {
				// Show user success message and clear form
				$scope.credentials = null;
				$scope.success = response.message;

			}).error(function(response) {
				// Show user error message and clear form
				$scope.credentials = null;
				$scope.error = response.message;
			});
		};

		// Change user password
		$scope.resetUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.passwordDetails = null;

				// Attach user profile
				Authentication.user = response;

				// And redirect to the index page
				$location.path('/password/reset/success');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);
'use strict';

angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', 'Users', 'Authentication',
	function($scope, $http, $location, Users, Authentication) {
		$scope.user = Authentication.user;

		// If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/');

		// Check if there are additional accounts 
		$scope.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in $scope.user.additionalProvidersData) {
				return true;
			}

			return false;
		};

		// Check if provider is already in use with current user
		$scope.isConnectedSocialAccount = function(provider) {
			return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
		};

		// Remove a user social account
		$scope.removeUserSocialAccount = function(provider) {
			$scope.success = $scope.error = null;

			$http.delete('/users/accounts', {
				params: {
					provider: provider
				}
			}).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.user = Authentication.user = response;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid) {
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);

				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response;
				}, function(response) {
					$scope.error = response.data.message;
				});
			} else {
				$scope.submitted = true;
			}
		};

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/users/password', $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.passwordDetails = null;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);
// 'use strict';

// // Authentication service for user variables
// angular.module('users').factory('Authentication', [
// 	function() {
// 		var _this = this;

// 		_this._data = {
// 			user: window.user
// 		};

// 		return _this._data;
// 	}
// ]);

'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', ['$window', function($window) {
  var auth = {
    user: $window.user,
    isAdmin: function () {
      return (($window.user.roles.indexOf('admin') !== -1) ?  true :  false);
    },
    isApprover: function () {
      return (($window.user.roles.indexOf('approver') !== -1) ?  true :  false);
    },
    isEditor: function () {
      return (($window.user.roles.indexOf('editor') !== -1) ?  true :  false);
    },
    isCreator: function () {
      return (($window.user.roles.indexOf('creator') !== -1) ?  true :  false);
    }
  };

  return auth;
}]);

'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
	function($resource) {
		return $resource('users', {}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);