'use strict';

// Categories controller
angular.module('categories').controller('CategoriesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Categories',
	function($scope, $stateParams, $location, Authentication, Categories) {
		$scope.authentication = Authentication;

		$scope.fields = [];

		//Redirect if not Registered
		if(!Authentication.user) {
			$state.go('home');
		}

		// Create new Category
		$scope.create = function() {
			// Create new Category object
			var category = new Categories (_.extend({
				name: this.name,
				permalink: this.permalink
			}, {fields: $scope.fields}));

			// Redirect after save
			category.$save(function(response) {
				$location.path('categories/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Add new Field
		$scope.addField = function(type) {
			switch(type){
				case 'text':
					$scope.fields.push({name: '', uri: '', type: 'text'});
					break;
				case 'textarea':
					$scope.fields.push({name: '', uri: '', type: 'textarea'});
					break;
				case 'number':
					$scope.fields.push({name: '', uri: '', type: 'number'});
					break;
				case 'selection':
					$scope.fields.push({name: '', uri: '', type: 'selection', selectiontype: 'single', selections: []});
					break;
				case 'image':
					$scope.fields.push({name: '', uri: '', type: 'image'});
					break;
				case 'derived':
					$scope.fields.push({name: '', uri: '', type: 'derived'});
					break;
			}
		};

		$scope.removeField = function(index){
			$scope.fields.splice(index, 1);
		};

		$scope.addSelection = function(index, value, label ){
			$scope.fields[index].toAddValue = '';
			$scope.fields[index].toAddLabel = '';
			$scope.fields[index].selections.push({value: value, label: label});
		};

		// Remove existing Category
		$scope.remove = function(category) {
			if ( category ) { 
				category.$remove();

				for (var i in $scope.categories) {
					if ($scope.categories [i] === category) {
						$scope.categories.splice(i, 1);
					}
				}
			} else {
				$scope.category.$remove(function() {
					$location.path('categories');
				});
			}
		};

		// Update existing Category
		$scope.update = function() {
			// Clean fields
			$scope.category = _.omit($scope.category,'fields');

			// var category = _.extend($scope.category, {fields: $scope.fields});
			var category = $scope.category;
			category.fields = $scope.fields;

			category.$update(function() {
				$location.path('categories/' + category._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Categories
		$scope.find = function() {
			$scope.categories = Categories.query();
		};

		// Find existing Category
		$scope.findOne = function() {
			$scope.category = Categories.get({ 
				categoryId: $stateParams.categoryId
			});

			$scope.category.$promise.then(function (result) {
			    $scope.fields = $scope.category.fields;
			});

		};

		$scope.updateURI = function(from, index){
			var re = /[^a-z0-9]+/gi;
	    var re2 = /^-*|-*$/g;
	    from = from.replace(re, '-').toLowerCase();
			eval('$scope.fields'+ '[' + index + '].uri = \'' + from + '\'');
		};

		$scope.updatePermalink = function(str, isNew) {
				if(str !== undefined){
			    var re = /[^a-z0-9]+/gi; // global and case insensitive matching of non-char/non-numeric
			    var re2 = /^-*|-*$/g;     // get rid of any leading/trailing dashes
			    str = str.replace(re, '-');  // perform the 1st regexp
			    // return str.replace(re2, '').toLowerCase(); // ..aaand the second + return lowercased result
			    if(isNew){
			    	$scope.permalink = '';
			    	$scope.permalink = str.replace(re2, '').toLowerCase(); // ..aaand the second + return lowercased result
			    } else {
			    	$scope.category.permalink = str.replace(re2, '').toLowerCase(); // ..aaand the second + return lowercased result
			    }
				} else {
					$scope.permalink = '';
				}
		};

		$scope.updateDerived = function(category){
			
		};
		
	}
]);