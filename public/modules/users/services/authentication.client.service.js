// 'use strict';

// // Authentication service for user variables
// angular.module('users').factory('Authentication', [
// 	function() {
// 		var _this = this;

// 		_this._data = {
// 			user: window.user
// 		};

// 		return _this._data;
// 	}
// ]);

'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', ['$window', function($window) {
  var auth = {
    user: $window.user,
    isAdmin: function () {
      return (($window.user.roles.indexOf('admin') !== -1) ?  true :  false);
    },
    isApprover: function () {
      return (($window.user.roles.indexOf('approver') !== -1) ?  true :  false);
    },
    isEditor: function () {
      return (($window.user.roles.indexOf('editor') !== -1) ?  true :  false);
    },
    isCreator: function () {
      return (($window.user.roles.indexOf('creator') !== -1) ?  true :  false);
    }
  };

  return auth;
}]);
