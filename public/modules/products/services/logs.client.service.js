'use strict';

//Products service used to communicate Products REST endpoints
// angular.module('products').factory('Products', ['$resource',
// 	function($resource) {
// 		return $resource('products/:productId', { productId: '@_id'
// 		}, {
// 			update: {
// 				method: 'PUT'
// 			}
// 		});
// 	}
// ]);

// 'use strict';

//Logs service used to communicate Logs REST endpoints
angular.module('products').factory('Logs', ['$resource',
  function($resource) {
    return $resource('logs/:logId', { logId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);