'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Product Schema
 */
var ProductSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Product name',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	image: {
    type: String,
    default: ''
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
},
	{ strict: false }
);

mongoose.model('Product', ProductSchema);