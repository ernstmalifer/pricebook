'use strict';

// Products controller
angular.module('products').controller('ProductsController', ['$scope', '$stateParams', '$state', '$timeout', '$sce', '$location', 'Authentication', 'Products', 'Categories', 'Logs',
	function($scope, $stateParams, $state, $timeout, $sce, $location, Authentication, Products, Categories, Logs) {
		$scope.authentication = Authentication;

		//Redirect if not Registered
		if(!Authentication.user) {
			$state.go('home');
		}

		// Create new Product
		$scope.create = function() {
			// Create new Product object
			var product = new Products ({
				name: this.name,
				category: this.category,
				status: this.status,
				image: this.image,
				price: this.price,
				sellprice: this.sellprice,
				fields: []
			});

			_.each($scope.fields, function(field){
				var fieldObject = {};
				fieldObject[field.uri] = field.value;
				product.fields.push(fieldObject);
				// _.extend(product, fieldObject);
			});

			product.$save(function(response) {
				// $location.path('products/' + response._id);
				$location.path('products/category/' + product.category);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Product
		$scope.remove = function(product) {
			if ( product ) { 
				product.$remove();

				for (var i in $scope.products) {
					if ($scope.products [i] === product) {
						$scope.products.splice(i, 1);
					}
				}
			} else {
				$scope.product.$remove(function() {
					$location.path('products');
				});
			}
		};

		// Update existing Product
		$scope.update = function() {
			var product = $scope.product;

			product.name = product.name;
			product.status = product.status;
			product.category = product.category;
			product.image = product.image;
			product.price = product.price;
			product.sellprice = product.sellprice;
			product.fields = [];

			_.each($scope.fields, function(field){
				var fieldObject = {};
				fieldObject[field.uri] = field.value;
				product.fields.push(fieldObject);
				// _.extend(product, fieldObject);
			});

			// console.log(product);

			product.$update(function() {
				// $location.path('products/' + product._id);
				$location.path('products/category/' + product.category);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Products
		$scope.find = function() {
			$scope.products = Products.query();
		};

		// Find a list of Categories
		$scope.findCategories = function() {
			$scope.categories = Categories.query();

			$scope.categories.$promise.then(function(resp){
				$scope.searchCategorySelected = {};
				$scope.searchCategorySelected.name = $scope.categories[0].permalink;

				if($stateParams.categoryId){
					var categoryId = _.find($scope.categories, function(category){return category.permalink == $stateParams.categoryId});
					$scope.searchCategorySelected.name = categoryId.permalink;
					$scope.searchCategorySelectedChanged();
				} else {
					$scope.searchCategorySelectedChanged();
				}

			});
		};

		// Find existing Product
		$scope.findOne = function() {
			$scope.product = Products.get({ 
				productId: $stateParams.productId
			});
			$scope.product.$promise.then(function(resp){
				$scope.category = $scope.product.category;
				$scope.updateCategory();

				_.each($scope.product.fields, function(field, index){

					var key = _.keys(field)[0];
					var value =_.values(field)[0];
					
					// Fuse to fields
					$scope.categories.$promise.then(function(resp){
						$scope.fields = _.each($scope.fields, function(field){ if(field.uri === key){ field.value = value; } });
					});
				});
			});


			$scope.logs = Logs.query({productId: $stateParams.productId});
		};

		$scope.updateCategory = function(){
			$scope.categories.$promise.then(function(resp){
				var categorySelected = _.find($scope.categories, function(category){return category.permalink === $scope.category;});
				$scope.fields = categorySelected.fields;
			});
		};

		$scope.derive = function(deriveFunction, index){
			var val = deriveFunction.substring(deriveFunction.lastIndexOf('<%= ')+4,deriveFunction.lastIndexOf(' %>'));

			var deriveFunctionReplaced = deriveFunction.replace(val,'val');

			var value = 0;
			// If 0 == price
			if(val === '0'){
				value = $scope.product.price;
			} else {
				value = $scope.fields[val-1].value;
			}

			if(!isNaN(value) && value.length !== 0){
				var compiled = _.template(deriveFunctionReplaced);
				$scope.fields[index].value = eval(compiled({val: value})).toFixed(2);
				return eval(compiled({val: value})).toFixed(2);
			} else {
				$scope.fields[index].value = '0.00';
				return '0.00';
			}
		};

		$scope.updateSellPrice = function(){
			$scope.sellprice = ($scope.price / (1 - .25)).toFixed(2);
		};

		$scope.updateSellPriceU = function(){
			$scope.product.sellprice = ($scope.product.price / (1 - .25)).toFixed(2);
		};

		$scope.onSuccess = function(resp, update){
			if(update){
				$scope.product.image = resp.data.filename;
				$scope.productimageupdate = true;
			} else {
				$scope.image = resp.data.filename;
			}
		};

		$scope.searchInit = function(){
			$scope.allFields = [];
			$scope.allUniqueFields = [];
			$scope.allDuplicateFields = [];

			

			$scope.products.$promise.then(function(resp){
				$scope.categories.$promise.then(function(resp){
					// For each category, get all fields and fuse
					_.each($scope.categories, function(category){
						_.each(category.fields, function(field){
							$scope.allFields.push(field.uri);
						});
					});

					// console.log($scope.allFields);
					// Remove duplicates
					$scope.allUniqueFields = _.uniq($scope.allFields);
					// console.log($scope.allUniqueFields);

					// Get duplicates
					var count = _.countBy($scope.allFields, _.identity);
					_.chain(count).keys().each(function(countitemkey){ if(count[countitemkey] > 1){ $scope.allDuplicateFields.push(countitemkey); }});
					// console.log($scope.allDuplicateFields);
					$scope.updateCriteria();

				});
			});
		};

		$scope.$on("slideEnded", function() {
		  $scope.updateCriteria();
		  $scope.$apply()
		});

		$scope.sprice = { min: 0, max: 200000};
		$scope.ssellprice = { min: 0, max: 200000};

		$scope.checkAll = function(fieldSelections){
			_.each(fieldSelections, function(fieldSelection){
				fieldSelection.selected = true;
			});
			$scope.updateCriteria();
		};

		$scope.unCheckAll = function(fieldSelections){
			_.each(fieldSelections, function(fieldSelection){
				fieldSelection.selected = false;
			});
			$scope.updateCriteria();
		};

		$scope.selectDeselectAllProducts = function(){
			_.each($scope.products, function(product){
				product.selected = $scope.productsSelectedAll;
			});
		};

		$scope.updateCriteria = function(){

			$scope.searchCriteria = { fields: [] };

			var selectedCategory = _.find($scope.categories, function(category){
				return category.permalink === $scope.searchCategorySelected.name;
			});

			_.each(selectedCategory.fields, function(field){
				if(field.searchable){
					switch(field.type){
						case 'text':
							$scope.searchCriteria.fields.push({name: field.uri, type: field.type, text: field.value})
							break;
						case 'number':
							$scope.searchCriteria.fields.push({name: field.uri, type: field.type, minval: parseInt(field.minval), maxval: parseInt(field.maxval)})
							break;
						case 'derived':
							$scope.searchCriteria.fields.push({name: field.uri, type: field.type, minval: parseInt(field.minval), maxval: parseInt(field.maxval)})
							break;
						case 'textarea':
							$scope.searchCriteria.fields.push({name: field.uri, type: field.type, text: field.value})
							break;
						case 'selection':
							var selections = [];
							_.each(field.selections, function(selection){
								if(selection.selected){ selections.push(selection); }
							})
							$scope.searchCriteria.fields.push({name: field.uri, type: field.type, selections: selections})
							break;
					}
				}
			});

			// console.log(selectedCategory, $scope.searchCriteria)

			// console.log($scope.searchCriteria)
			// $scope.searchCriteria = searchCriteria;
		};

		$scope.categoryFilter = function(product){
			if($scope.searchCategorySelected){
				return product.category === $scope.searchCategorySelected.name;
			} else {
				return false;
			}
		};

		$scope.sortBy = function(predicate, index){
      $scope.products.sort(function(x,y){

      	// Sort by fields
      	if(index !== undefined) {
      		var result = compareValues(x.fields[index][predicate], y.fields[index][predicate]);
	      	return result === 0
	      		? compareValues(y.fields[index][predicate], x.fields[index][predicate])
	      		: result;
      	} else {
      		var result = compareValues(x[predicate], y[predicate]);
	      	return result === 0
	      		? compareValues(y[predicate], x[predicate])
	      		: result;
      	}
      	

      })

      // console.log($scope.products)
    }

    function compareValues(x, y){
    	return (x > y)
    		? 1
    		: (x < y ? -1 : 0);
    };

		$scope.criteriaFilter = function(product){

			var flag = true;

			if(product === undefined){ return false; }

			if(!isNaN($scope.sprice.minval) && !isNaN($scope.sprice.maxval)){
				if(product.price < $scope.sprice.minval){
					flag = false;
				}
				if(product.price > $scope.sprice.maxval){
					flag = false;
				}
			}

			if(!isNaN($scope.ssellprice.minval) && !isNaN($scope.ssellprice.maxval)){
				if(product.sellprice < $scope.ssellprice.minval){
					flag = false;
				}
				if(product.sellprice > $scope.ssellprice.maxval){
					flag = false;
				}
			}


			_.each($scope.searchCriteria.fields, function(field){

				var uriToCompare = field.name;
				var uriToCompareType = field.type;


				var productFieldToCompare = _.find(product.fields, function(productfield){
					return uriToCompare === _.keys(productfield)[0];
				});

				// console.log(field)

				switch(uriToCompareType) {
					case 'image':
						break;
					case 'text':
						break;
					case 'description':
						break;
					case 'selection':
					// console.log(field.selections, productFieldToCompare)
					// console.log(_.contains(_.pluck(field.selections, 'value'), productFieldToCompare[uriToCompare]))
						if(!_.isEmpty(field.selections)){
							if(!_.contains(_.pluck(field.selections, 'value'), productFieldToCompare[uriToCompare])){
								// console.log(_.pluck(field.selections, 'value'), productFieldToCompare[uriToCompare]);
								flag = false;
							}
						} else {
							flag = false;
						}
						break;
					case 'number':
					// console.log(isNaN(field.minval))
						if(!isNaN(field.minval) && !isNaN(field.maxval)){
							if(productFieldToCompare[uriToCompare] < field.minval){
								// console.log('2');
								flag = false;
							}
							if(productFieldToCompare[uriToCompare] > field.maxval){
								// console.log('3');
								flag = false;
							}
						}
						break;
					case 'derived':
						if(!isNaN(field.minval) && !isNaN(field.maxval)){
							if(productFieldToCompare !== undefined){
								if(productFieldToCompare[uriToCompare] < field.minval){
									// console.log('4');
									flag = false;
								}
								if(productFieldToCompare[uriToCompare] > field.maxval){
									// console.log('5');
									flag = false;
								}
							} else {
								flag = true;
							}
							
						}
						break;
					default:
				}

			})

			// console.log(flag);
			return flag;
		}

		$scope.searchCategorySelectedChanged = function(){
			$scope.categories.$promise.then(function(resp){
				$scope.columns = [];
				var category = _.find($scope.categories, function(category){ return category.permalink === $scope.searchCategorySelected.name; })
				_.chain(category.fields).pluckMany('name','uri').each(function(field){
					var selected = true;
					if(field.uri === 'specifications' || field.uri === 'notes' || field.uri === 'warranty-support' || field.uri === 'description') {
						selected = false;
					}
					$scope.columns.push({name: field.name, uri: field.uri, selected: selected});
				})
			})
		};

		$scope.setProductRowClass = function(status){
			switch(status){
				case 'updated':
					return 'danger';
				case 'for approval':
					return 'warning';
				case 'approved':
					return '';
			}
		};

		$scope.setProductRowTag = function(status){
			switch(status){
				case 'updated':
					return 'danger';
				case 'for approval':
					return 'warning';
				case 'approved':
					return 'success';
			}
		};

		$scope.isColumnShown = function(key){
			var column = _.find($scope.columns, function(column){ return _.keys(key)[0] === column.uri;})
			if(column !== undefined){
				return column.selected;
			} else {
				return false;
			}
		};

		$scope.getColumnValue = function(product, fielduri){
			// console.log(product.fields, fielduri)
			var fieldObject = _.find(product.fields, function(field){
				return _.keys(field)[0] === fielduri
			});
			return _.values(fieldObject)[0];
		}

		$scope.formatField = function(field){

			var key = _.keys(field)[0];

			var selectedCategory = _.find($scope.categories, function(category){
				return category.permalink === $scope.searchCategorySelected.name;
			});

			var selectedField = _.find(selectedCategory.fields, function(field){
				return field.uri == key;
			})

			if(field){
				switch(selectedField.type) {
					case 'description':
						return '<small>' + field[key] + '</small>';
					case 'number':
						var returnNumber = '';
						if(selectedField.prefix){ returnNumber += selectedField.prefix; }
						returnNumber += field[key];
						if(selectedField.postfix){ returnNumber += selectedField.postfix; }
						
						return returnNumber;
					case 'derived':
						var returnNumber = '';
						if(selectedField.prefix){ returnNumber += selectedField.prefix; }
						returnNumber += field[key];
						if(selectedField.postfix){ returnNumber += selectedField.postfix; }
						
						return returnNumber;

					default:
						return field[key];
				}
			}
		};

		$scope.updatedFormat = function(date){
			return moment(date).fromNow();
		}

		$scope.formatLog = function(updates){
			var string = '';
			_.each(updates, function(update, index){
				string += update.key;
				if(index == updates.length - 1){
					string += ''
				} else {
					string += ', '
				}
			});

			return string;
		}

		$scope.go = function ( path ) {
		  $location.path( path );
		};

	}
		
]);

String.prototype.capitalize = function() {
    return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};

// Pluck extend
_.mixin({ 
    pluckMany: function() {
        var args = _.rest(arguments, 1);
        return _.map(arguments[0], function(item) {
            var obj = {};
            _.each(args, function(arg) {
                obj[arg] = item[arg]; 
            });
            return obj;
        });
    }
})