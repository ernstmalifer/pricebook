'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Product = mongoose.model('Product'),
	Category = mongoose.model('Category'),
	Log = mongoose.model('Log'),
	fs = require('fs'),
	_ = require('lodash'),
	mv = require('mv'),
	ObjectId = mongoose.Types.ObjectId; 

/**
 * Create a Product
 */
exports.create = function(req, res) {
	var product = new Product(req.body);
	product.user = req.user;
	product.updated = new Date();

	product.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {

			// Move file
			if(product.toObject().image){
				mv('public/uploads/temp/' + product.toObject().image, 'public/uploads/' + product.toObject().image, function(err) {
					if(err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
					}
				});
			}

			logthis(req, 'product_creation', {product: product});
			res.jsonp(product);
		}
	});
};

/**
 * Show the current Product
 */
exports.read = function(req, res) {
	res.jsonp(req.product);
};

/**
 * Update a Product
 */
exports.update = function(req, res) {
	// var product = req.product ;

	// product = _.extend(product , req.body);

	// product.save(function(err) {
	// 	if (err) {
	// 		return res.status(400).send({
	// 			message: errorHandler.getErrorMessage(err)
	// 		});
	// 	} else {
	// 		res.jsonp(product);
	// 	}
	// });


	var product = new Product(req.body);
	var oldProductInfo = {};
	Product.findById(req.body._id).exec(function(err, product) {
		oldProductInfo = product.toObject();
	});

	// Convert the Model instance to a simple object using Model's 'toObject' function
	// to prevent weirdness like infinite looping...
	var upsertData = product.toObject();
	upsertData.updated = new Date();

	// Delete the _id property, otherwise Mongo will return a "Mod on _id not allowed" error
	delete upsertData._id;

	Product.update({_id: product.id}, upsertData, {upsert: true},
		function(err){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {

				var productFieldsObject = product.toObject().fields;
				var updates = [];

				_.each(productFieldsObject, function(field, n){
					var key = _.keys(field)[0];
					var value = _.values(field)[0]

					var oldProductField = _.find(oldProductInfo.fields, function(field) { return _.keys(field)[0] == key;});

					if(_.values(oldProductField)[0], value !== _.values(oldProductField)[0]){
						updates.push({key: key, oldValue: _.values(oldProductField)[0], newValue: value})
					}
				})

				if(updates.length != 0){
					logthis(req, 'product_update', {product: product, updates: updates});
				}

				var productStatus = product.toObject().status;
				if(productStatus != oldProductInfo.status){
					logthis(req, 'product_changestatus', {product: product, oldValue: oldProductInfo.status, newValue: productStatus});
				}

				// Move file
				// console.log(product.toObject().image, oldProductInfo.image)
				if(product.toObject().image != oldProductInfo.image){
					mv('public/uploads/temp/' + product.toObject().image, 'public/uploads/' + product.toObject().image, function(err) {
						if(err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {

							//Remove old file
							try{
								fs.unlink('public/uploads/' + oldProductInfo.image, function (err) {
							  	if (err) throw err;
								});
							} catch(e){

							}
						}
					});
				}

				res.jsonp(product);
			}
		}
	);
};

/**
 * Delete an Product
 */
exports.delete = function(req, res) {
	var product = req.product ;

	product.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(product);
		}
	});
};

/**
 * List of Products
 */
exports.list = function(req, res) { 
	Product.find().sort('-created').populate('user', 'displayName').exec(function(err, products) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(products);
		}
	});
};

/**
 * Product middleware
 */
exports.productByID = function(req, res, next, id) { 
	Product.findById(id).populate('user', 'displayName').exec(function(err, product) {
		if (err) return next(err);
		if (! product) return next(new Error('Failed to load Product ' + id));
		req.product = product ;
		next();
	});
};

/**
 * Product authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	// if (req.product.user.id !== req.user.id) {
	// 	return res.status(403).send('User is not authorized');
	// }
	next();
};

/**
 * Create file upload
 */
exports.upload = function (req, res, next) {
	var file = req.files.file;
	var filename = Date.now() + '.jpg';

	mv(file.path, 'public/uploads/temp/' + filename, function(err) {
		if(err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			_.extend(file, {filename: filename});
			res.jsonp(file);
		}
	});

};

/**
 * Update Derived
 */
exports.updateDerived = function (req, res, next) {

	// console.log(req.params.categoryId);
	Category.findOne({'_id': new ObjectId(req.params.categoryId)}).exec(function(err, category) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {

			var categoryObj = category.toObject();

			_.each(categoryObj.fields, function(field){

				if(field.type === 'derived'){

					var val = field.derivedValue.substring(field.derivedValue.lastIndexOf('<%= ')+4,field.derivedValue.lastIndexOf(' %>'));
					var deriveFunctionReplaced = field.derivedValue.replace(val,'val');

					// Search products by category
					Product.find({'category': categoryObj.permalink}).exec(function(err, products) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							console.log('-------------start field ' + val + '----------');
							_.each(products, function(product){

								var productObj = product.toObject();
								var value = 0;

								if(val === '0'){
									value = productObj.price;
								} else {
									value = _.values(productObj.fields[val-1])[0];
								}

								// console.log(field.uri)
								var hasValueIndex = 0;
								_.find(productObj.fields, function(productfield, index){
									return _.keys(productfield)[0] === field.uri;
								});

								if(!isNaN(value) && value.length !== 0){
									var compiled = _.template(deriveFunctionReplaced);
									// $scope.fields[index].value = eval(compiled({val: value})).toFixed(2);
									// return eval(compiled({val: value})).toFixed(2);
									var fielda = {};
									fielda[field.uri] = eval(compiled({val: value})).toFixed(2);
									// console.log( fielda );

									// console.log( productObj );
									// console.log( productObj.fields );
									// console.log( _.keys(productObj.fields) );
									// console.log( _.keys(productObj.fields).indexOf(field.uri) );

									var keysall = [];
									_.each(productObj.fields, function(ar){keysall.push(_.keys(ar)[0]);});

									// console.log(keysall.indexOf(field.uri), field.uri)

									// Doesn't exists
									if( keysall.indexOf(field.uri) == -1 ) {
										// Push to array
										console.log( 'notarray' );
										productObj.fields.push(fielda);
										// console.log(productObj);
										// return res.status(400).send({
										// 		message: errorHandler.getErrorMessage(err)
										// 	});
									} else {
										// Splice and push
										console.log( 'notinarray' );
										productObj.fields.splice(keysall.indexOf(field.uri), 1);
										productObj.fields.push(fielda)
									}

									console.log( productObj );
									delete productObj._id;
									// console.log( productObj );

									Product.update({_id: product.id}, productObj, {upsert: true},
										function(err){
											if (err) {
												console.log('err');
												return res.status(400).send({
													message: errorHandler.getErrorMessage(err)
												});
											}
										}
									);

									// return res.status(400).send({
									// 			message: errorHandler.getErrorMessage(err)
									// 		});

								} else {
									// $scope.fields[index].value = '0.00';
									// console.log('0.00')
									// return '0.00';
								}



							})
						
							console.log('-------------end field----------');
						}
					});

				}
			});
		}
	});
};

function logthis(req, type, details){
	var log = new Log({type: type, details: details});
	log.user = req.user;
	log.save();
}
